#!/bin/sh

#########
# This file contain useful shell aliases.
# Every alias, function of sorts, starts with "vj-"-prefix,
# to differate the functions if any other actual function 
# should share its the name with any actual function.
#
# made by Vate, Valtteri Jokinen
#########


###############
# Copying aliases
# use rsync to see total progress, per file progress and copy full directories too
# requires $SOURCE and $DESTINATION arguments
alias vj-copy="rsync -ah --progress --info=progress2 "
#
# Optional with nice command, to restrict cpu usage. For background copying
alias vj-nice-copy="nice -n 13 rsync -ah --progress --info=progress2 "
#
###############
