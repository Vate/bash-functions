#!/bin/bash

#########
# Script that extracts audio stream from file. Requires ffmpeg.
# 
# made by Vate 
#########

source ./basic-bash-helper-functions.sh

vj-extract-audio ()
{
	vj-check-commands ffprobe ffmpeg PULLATUKKUITUTU

	# Get current audio stream format from argument "$1"
	local audioformat=$(ffprobe -loglevel error -select_streams a:0 -show_entries stream=codec_name -of default=nw=1:nk=1 "$1")

	echo $audioformat

	case $audioformat in
	"vorbis")
		audioformat="ogg"
        	;;
	*)
        	# audio format is okay
	esac

	# Extract the audio stream from input file argument
	# TODO, the original file extension from input file should be removed, if there is one, from the output filename
	ffmpeg -i "$1" -vn -acodec copy "$1.$audioformat"

}
