#!/bin/bash

#########
# This file contain useful shell functions, which can be used to 
# make other functions more structured. 
# Every self created function starts with "vj-"-prefix,
# to differate the functions if any other actual function 
# should share its the name with any actual function.
# 
# made by Vate 
#########



# CHECK FOR A COMMANDS
# Check if a command, program, works and is installed
vj-check-commands ()
{
	if [[ "$#" -eq "0" ]]; then # if not given argument
		echo "USAGE: \"vj-check-commands cd pwd mv\""	
	fi

	local COMMAND
	local NOT_FOUND=false

	# Loop through all given commands
	for COMMAND in "$@"
	do
	{
		command -v "$COMMAND" >/dev/null 2>&1 || 
		{ 
			echo >&2 "Command '$COMMAND' is required but it's not installed."; 
			NOT_FOUND=true;	 
		}
#if  ! ( command -v "$COMMAND" >/dev/null 2>&1 ); then
#		echo "NO" ;
#fi
	}
	done

	if [ "$NOT_FOUND" = true ] ; then
		# if even single command was not found		
		return 1		
	else
		return 0
	fi

}
