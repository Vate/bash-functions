# This project contains various useful BASH and SHELL -scripts. 
#
# To get these aliases or functions to your local shell session, the file
# has to be sourced in ".bash_profile", ".profile", ".bashrc" or equivalent
# file in home-directory with a following like command: 
#
#
#  FILE="/path/to/bash_functions"
#  if [[ -f $FILE ]]; then 
#      source $FILE
#  fi
#  unset FILE
#
#
# To source all .sh files from a folder, one can source with for loop like so: 
#
#  for f in "/path/to/bash_functions/*.sh"; do source $f; done
#
# It is recommended to source all files in .bashrc -file, 
# to get them in all types of shell sessions. 
#
# Made by Vate.
